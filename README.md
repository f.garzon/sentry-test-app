# README

Sample project to test Sentry with rails 7.0


```
SENTRY_DSN=<your sentry.io DSN> rails server
```

Navigate to <http://localhost:3000>, you will see a basic page, and a Sentry event `HELLO SENTRY`` should have been sent to <https://sentry.io>

```console
$ SENTRY_DSN=<your DSN> rails server
=> Booting Puma
=> Rails 7.0.6 application starting in development
=> Run `bin/rails server --help` for more startup options
Puma starting in single mode...
* Puma version: 5.6.6 (ruby 3.2.2-p53) ("Birdie's Version")
*  Min threads: 5
*  Max threads: 5
*  Environment: development
*          PID: 3101
* Listening on http://127.0.0.1:3000
* Listening on http://[::1]:3000
Use Ctrl-C to stop
Started GET "/" for ::1 at 2023-07-03 18:58:26 +0200
[Tracing] Starting <http.server> transaction </>
Processing by ArticlesController#index as HTML
[Transport] Sending envelope with items [event] d6ad1c69e3b4448ca392bfaf485c4367 to Sentry
  Rendering layout layouts/application.html.erb
  Rendering articles/index.html.erb within layouts/application
  Rendered articles/index.html.erb within layouts/application (Duration: 2.2ms | Allocations: 337)
  Rendered layout layouts/application.html.erb (Duration: 157.8ms | Allocations: 34113)
Completed 200 OK in 203ms (Views: 167.5ms | ActiveRecord: 0.0ms | Allocations: 74600)


[Transport] Sending envelope with items [transaction] 933560c3e15a467fb057f0dadf6385f3 to Sentry
[Transport] Sending envelope with items [sessions]  to Sentry
```

