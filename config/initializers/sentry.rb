Sentry.init do |config|
  # Use SENTRY_DSN to activate sentry in this app
  # config.dsn = 'https://d5143b243c6943cc9b3dbec6e0166989@o555013.ingest.sentry.io/6703169'
  config.breadcrumbs_logger = [:active_support_logger, :http_logger]

  # To activate performance monitoring, set one of these options.
  # We recommend adjusting the value in production:
  config.traces_sample_rate = 1.0
  # or
  config.traces_sampler = lambda do |context|
    true
  end
end